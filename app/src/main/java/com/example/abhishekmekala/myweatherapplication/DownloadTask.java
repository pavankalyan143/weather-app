package com.example.abhishekmekala.myweatherapplication;

import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
//import java.io.InputStreamReader;
import java.net.HttpURLConnection;
//import java.net.MalformedURLException;
import java.net.URL;

public class DownloadTask extends AsyncTask<String,Void,String> {
    @Override
    protected String doInBackground(String... strings) {
        String r="";
        URL url;
        HttpURLConnection httpurl=null;
        try {
            url=new URL(strings[0]);
            httpurl=(HttpURLConnection)url.openConnection();
            InputStream is=httpurl.getInputStream();
            InputStreamReader isr=new InputStreamReader(is);
            int d=isr.read();
            while (d!=-1)
            {
                char c=(char) d;
                r+=c;
                d=isr.read();
            }
            return r;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String r) {
        //super.onPostExecute(r);
        try {
            JSONObject jsonObject=new JSONObject(r);
            JSONObject climate=new JSONObject(jsonObject.getString("main"));
            double t=Double.parseDouble(climate.getString("temp"));
            int te=(int) (t-273.15);
            String place=jsonObject.getString("name");
            MainActivity.place.setText(place);
            MainActivity.temp.setText(String.valueOf(te));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
